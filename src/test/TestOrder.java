package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Order;
import datasource.*;

@WebServlet("/testorder")
public class TestOrder extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        OrderMapper om = new OrderMapper();
        Order order = om.findWithId(1, true);
        if (order == null) {
            out.println("<h1> no such order! </h1>");
        } else {
            boolean isExpress = order.isExpress(false);
            int weight = order.getOrderDetail(false).getWeight(false);
            String city = order.getDestination(false).getCity();
            out.println("<h1>" + isExpress + "</h1>");
            out.println("<h1>" + weight + "</h1>");
            out.println("<h1>" + city + "<h1>");
        }
    }
}
