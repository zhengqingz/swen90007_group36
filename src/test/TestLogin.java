package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Destination;
import service.RegisterAndLoginService;

@WebServlet("/testlogin")
public class TestLogin extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        RegisterAndLoginService service = new RegisterAndLoginService();
        boolean loginsuccess = service.login("client1", "client1");
        if(loginsuccess) {
            out.println("<h1> login success! </h1>");
        } else {
            out.println("<h1> login fail! </h1>");
        }
    }

}
