package service;

import java.util.ArrayList;
import java.util.List;

import datasource.ClientMapper;
import datasource.DestinationMapper;
import datasource.ExpressOrderMapper;
import datasource.NormalOrderMapper;
import domain.Client;
import domain.Destination;
import domain.ExpressOrder;
import domain.NormalOrder;
import domain.Order;
import domain.OrderDetail;

public class AddNewOrderService {

    public void addNewOrder(boolean isExpress, boolean isRegistered,
            int weight, boolean isFragile, int clientId, String city) {
        ClientMapper clientMapper = new ClientMapper();
        Client client = clientMapper.findInMap(clientId);
        Destination destination = addNewDestination(city);
        OrderDetail orderDetail = new OrderDetail(0, weight, isFragile);

        Order order = new NormalOrder(0, isExpress, client, orderDetail,
                destination, isRegistered);

        NormalOrderMapper normalOrderMapper = new NormalOrderMapper();
        int orderId = normalOrderMapper.insert(order);
        order.setOrderId(orderId);
        orderDetail.setOrderDetailId(orderId);
        addClientDestination(client, destination);
        // IdentityMap.getInstance(order).put(orderId, order);
        // IdentityMap.getInstance(orderDetail).put(orderId, orderDetail);
    }

    public void addNewOrder(boolean isExpress, int days, int weight,
            boolean isFragile, int clientId, String city) {
        ClientMapper clientMapper = new ClientMapper();
        Client client = clientMapper.findInMap(clientId);
        Destination destination = addNewDestination(city);
        OrderDetail orderDetail = new OrderDetail(0, weight, isFragile);

        Order order = new ExpressOrder(0, isExpress, client, orderDetail,
                destination, days);

        ExpressOrderMapper normalOrderMapper = new ExpressOrderMapper();
        int orderId = normalOrderMapper.insert(order);
        order.setOrderId(orderId);
        orderDetail.setOrderDetailId(orderId);
        addClientDestination(client, destination);
        // IdentityMap.getInstance(order).put(orderId, order);
        // IdentityMap.getInstance(orderDetail).put(orderId, orderDetail);
    }

    private Destination addNewDestination(String city) {
        DestinationMapper destiMapper = new DestinationMapper();
        Destination temp = destiMapper.findWithCity(city);
        if (temp == null) {
            Destination destination = new Destination(0, city);
            int destinationId = destiMapper.insert(destination);
            destination.setDestinationId(destinationId);
            // IdentityMap.getInstance(destination).put(destinationId,
            // destination);
        }
        return temp;
    }

    private void addClientDestination(Client client, Destination destination) {
        ClientMapper clientMapper = new ClientMapper();
        List<Destination> destinations = new ArrayList<>();
        destinations = clientMapper.loadDestinations(client.getClientId());
        boolean alreadyHas = false;
        for (Destination d : destinations) {
            if (d.getCity().equalsIgnoreCase(destination.getCity())) {
                alreadyHas = true;
                break;
            }
        }
        if (!alreadyHas) {
            clientMapper.insert(client, destination);
        }
    }

}
