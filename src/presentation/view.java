package presentation;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datasource.OrderMapper;
import domain.Order;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Servlet implementation class ViewBooks
 */
@WebServlet("/view")
public class view extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int orderid = Integer.parseInt(request.getParameter("orderid"));
		OrderMapper om = new OrderMapper();
		Order order = om.findWithId(orderid, true);

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>");
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\">");
		out.println("<title></title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h2>Order Details</h2>");
		out.println("Tracking No: <input type=\"text\" name=\"trackingnumber\" value=\"" + order.getOrderId()
				+ "\" disabled ><br>");
		out.println("Destination: <input type=\"text\" name=\"destination\" value=\""
				+ order.getDestination(false).getCity() + "\" disabled><br>");
		out.println("Weight: <input type=\"text\" name=\"weight\" value=\""
				+ order.getOrderDetail(false).getWeight(false) + "\" disabled>kg<br>");
		out.println("Fragile: <input type=\"text\" name=\"fragile\" value=\""
				+ order.getOrderDetail(false).isFragile(false) + "\" disabled><br>");
		out.println(
				"<button type=\"orderlist\" onclick=\"alert('OrderList')\"class=\"button button2\">Order List</button>");
		out.println(
				"<button type=\"neworder\" onclick=\"alert('neworder')\"class=\"button button2\">New Order</button>");
		out.println("<link rel='stylesheet' href='style.css'/>");
		out.println("</body>");
		out.println("</html>");

		out.println("<link rel='stylesheet' href='style.css'/>");
		out.println("</body>");
		out.println("</html>");

		out.close();
	}
}
