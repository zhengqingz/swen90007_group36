package domain;

import datasource.ClientMapper;
import datasource.IdentityMap;

public class Client {

    private int clientId;
    private String username;
    private String password;

    public Client() {
    }
    
    public Client(int clientId) {
        this.clientId = clientId;
    }

    public Client(int clientId, String username, String password) {
        this.clientId = clientId;
        this.username = username;
        this.password = password;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        if (username == null) {
            load();
        }
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        if (password == null) {
            load();
        }
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void load() {
        ClientMapper clientMapper = new ClientMapper();
        Client temp = clientMapper.findWithId(clientId, false);
        username = temp.getUsername();
        password = temp.getPassword();
        IdentityMap.getInstance(this).put(clientId, this);
    }

}
