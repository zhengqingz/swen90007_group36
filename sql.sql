DROP TABLE APP.client;
DROP TABLE APP.destination;
DROP TABLE APP.orders;
DROP TABLE APP.client_has_destination;

CREATE TABLE APP.client (
   idClient       INT GENERATED ALWAYS AS IDENTITY,
   username       VARCHAR(45),
   password       VARCHAR(45),
   PRIMARY KEY (idClient)
);

CREATE TABLE APP.destination (
   idDestination   INT GENERATED ALWAYS AS IDENTITY,
   city            VARCHAR(45),
   PRIMARY KEY (idDestination)
);

CREATE TABLE APP.orders (
   idOrder         INT GENERATED ALWAYS AS IDENTITY,
   isExpress       BOOLEAN,
   isRegistered    BOOLEAN,
   days            INT,
   type            VARCHAR(45),
   weight          INT,
   isFragile       BOOLEAN,   
   client_id       INT,
   destination_id  INT,
   PRIMARY KEY (idOrder),
   FOREIGN KEY (client_id) REFERENCES APP.client(idClient),
   FOREIGN KEY (destination_id) REFERENCES APP.destination(idDestination)    
);

CREATE TABLE APP.client_has_destination (
   id                INT GENERATED ALWAYS AS IDENTITY,
   client_id         INT,
   destination_id    INT,
   PRIMARY KEY (id, client_id,destination_id),
   FOREIGN KEY (destination_id) REFERENCES APP.destination(idDestination),
   FOREIGN KEY (client_id) REFERENCES APP.client(idClient)
);


INSERT INTO APP.client (username, password)
VALUES ('client1', 'client1');
INSERT INTO APP.client (username, password)
VALUES ('client2', 'client2');

INSERT INTO APP.destination (city)
VALUES ('Melbourne');
INSERT INTO APP.destination (city)
VALUES ('Beijing');

--(isExpress,isRegistered,days,type,weight,isFragile,client_id,destination_id)
INSERT INTO APP.orders (isExpress,days,type,weight,isFragile,client_id,destination_id)
VALUES (true,2,'express',5,false,1,1);
INSERT INTO APP.orders (isExpress,isRegistered,type,weight,isFragile,client_id,destination_id)
VALUES (false,true,'normal',3,true,1,2);
INSERT INTO APP.orders (isExpress,days,type,weight,isFragile,client_id,destination_id)
VALUES (true,1,'express',4,true,2,1);
INSERT INTO APP.orders (isExpress,isRegistered,type,weight,isFragile,client_id,destination_id)
VALUES (false,false,'normal',6,false,2,2);

INSERT INTO APP.client_has_destination (client_id,destination_id)
VALUES (1,1);
INSERT INTO APP.client_has_destination (client_id,destination_id)
VALUES (1,2);
INSERT INTO APP.client_has_destination (client_id,destination_id)
VALUES (2,1);
INSERT INTO APP.client_has_destination (client_id,destination_id)
VALUES (2,2);
