package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datasource.*;

@WebServlet("/test")
public class Test extends HttpServlet {

    private final static String findUserWithID = "SELECT * "
            + "  from APP.destination " + "  WHERE idDestination = ?";

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        String result = "";
        try {
            findStatement = DBConnection.prepare(findUserWithID);
            findStatement.setInt(1, 2);
            rs = findStatement.executeQuery();
            out.println("<h1>" + rs + " </h1>");
            rs.next();
            result = rs.getString(2);
        } catch (SQLException e) {
        }
        out.println("<h1>" + result + " </h1>");
    }

}
