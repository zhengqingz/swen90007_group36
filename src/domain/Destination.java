package domain;

import datasource.DestinationMapper;
import datasource.IdentityMap;

public class Destination {

    private int destinationId;
    private String city;

    public Destination() {

    }

    public Destination(int destinationId) {
        this.destinationId = destinationId;
    }

    public Destination(int destinationId, String city) {
        this.destinationId = destinationId;
        this.city = city;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public String getCity() {
        if (city == null) {
            load();
        }
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private void load() {
        DestinationMapper destinationMapper = new DestinationMapper();
        Destination temp = destinationMapper.findWithId(destinationId, false);
        city = temp.getCity();
        IdentityMap.getInstance(this).put(destinationId, this);
    }

}
