package domain;

import datasource.IdentityMap;
import datasource.OrderMapper;
import datasource.UnitOfWork;

public abstract class Order {

    protected int orderId;
    protected boolean isExpress;
    protected Client client;
    protected OrderDetail orderDetail;
    protected Destination destination;

    public Order() {

    }

    public Order(int orderId) {
        this.orderId = orderId;
    }

    public Order(int orderId, boolean isExpress, Client client,
            OrderDetail orderDetail, Destination destination) {
        this.orderId = orderId;
        this.isExpress = isExpress;
        this.client = client;
        this.orderDetail = orderDetail;
        this.destination = destination;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isExpress(boolean isInserting) {
        if (!isInserting) {
            checkAndLoad();
        }
        return isExpress;
    }

    public void setExpress(boolean isExpress) {
        this.isExpress = isExpress;
    }

    public Client getClient(boolean isInserting) {
        if (!isInserting) {
            checkAndLoad();
        }
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public OrderDetail getOrderDetail(boolean isInserting) {
        if (!isInserting) {
            checkAndLoad();
        }
        return orderDetail;
    }

    public void setOrderDetail(OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
        UnitOfWork.getInstance().registerDirty(this);
    }

    public Destination getDestination(boolean isInserting) {
        if (!isInserting) {
            checkAndLoad();
        }
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    protected void checkAndLoad() {
        if (client == null || orderDetail == null || destination == null) {
            OrderMapper orderMapper = new OrderMapper();
            Order temp = orderMapper.findWithId(orderId, false);
            isExpress = temp.isExpress(false);
            client = temp.getClient(false);
            destination = temp.getDestination(false);
            orderDetail = temp.getOrderDetail(false);
            IdentityMap.getInstance(this).put(orderId, this);
        }
    }

}
