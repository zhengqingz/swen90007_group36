package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datasource.ClientMapper;
import domain.Client;
import service.RegisterAndLoginService;

@WebServlet("/loginserv")
public class LoginServ extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		PrintWriter out = response.getWriter();
		RegisterAndLoginService service = new RegisterAndLoginService();
		boolean loginsuccess = service.login(username, password);
		if (loginsuccess) {
			out.println("<h1> login successful! </h1>");
			RequestDispatcher rd=request.getRequestDispatcher("order");
			rd.forward(request, response);
		} else {
			out.println("<h1> login fail! </h1>");
			out.println("<a href=\"index.html\">Back to Login Page.</a>");
		}
	}
}