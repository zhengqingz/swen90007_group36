package datasource;

import java.util.ArrayList;
import java.util.List;

import domain.Order;

public class UnitOfWork {
    // private static ThreadLocal current = new ThreadLocal<>();
    private static UnitOfWork instance;
    private List<Order> newOrders = new ArrayList<Order>();
    private List<Order> dirtyOrders = new ArrayList<Order>();
    private List<Order> deletedOrders = new ArrayList<Order>();

    public static synchronized UnitOfWork getInstance() {
        if (instance == null) {
            instance = new UnitOfWork();
        }
        return instance;
    }

    // public static void newCurrent() {
    // setCurrent(new UnitOfWork());
    // }

    // public static void setCurrent(UnitOfWork uow) {
    // current.set(uow);
    // }

    // public static UnitOfWork getCurrent() {
    // return (UnitOfWork) current.get();
    // }

    public void registerNew(Order order) {
        newOrders.add(order);
    }

    public void registerDirty(Order order) {
        assert order.getOrderId() != 0 : "id is 0";
        assert !deletedOrders.contains(order) : "object is deleted";
        if (!dirtyOrders.contains(order) && !newOrders.contains(order)) {
            dirtyOrders.add(order);
        }
    }

    public void registerDeleted(Order order) {
        assert order.getOrderId() != 0 : "id is 0";
        if (newOrders.remove(order)) {
            return;
        }
        dirtyOrders.remove(order);
        if (!deletedOrders.contains(order)) {
            deletedOrders.add(order);
        }
    }

    public void registerClean(Order order) {
        assert order.getOrderId() != 0 : "id is 0";
    }

    public void commit() {
        OrderMapper om = new OrderMapper();
        for (Order order : newOrders) {
            if (order.isExpress(false)) {
                ExpressOrderMapper eom = new ExpressOrderMapper();
                eom.insert(order);
            } else {
                NormalOrderMapper nom = new NormalOrderMapper();
                nom.insert(order);
            }
        }
        for (Order order : dirtyOrders) {
            om.update(order);
        }
        for (Order order : deletedOrders) {
            om.delete(order);
        }
    }
}