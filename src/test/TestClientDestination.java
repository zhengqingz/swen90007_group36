package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Destination;
import datasource.*;

@WebServlet("/testclientdestination")
public class TestClientDestination extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        ClientMapper cm = new ClientMapper();
        List<Destination> destinations = new ArrayList<>();
        destinations = cm.loadDestinations(1);
        Iterator<Destination> it = destinations.iterator();
        while (it.hasNext()) {
            Destination destination = it.next();
            out.println("<h1>" + destination.getCity() + " </h1>");
        }
    }

}
