package test;

import java.io.IOException;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Order;
import service.DeleteOrderService;

import datasource.*;

@WebServlet("/testorderdelete")
public class TestOrderDelete extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        OrderMapper om = new OrderMapper();
        Order order = om.findWithId(5, true);
        DeleteOrderService dos = new DeleteOrderService();
        dos.deleteOrder(order);
    }
}
