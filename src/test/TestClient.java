package test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import datasource.*;

@WebServlet("/testclient")
public class TestClient extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        ClientMapper cm = new ClientMapper();
        Client client = cm.findWithUsername("client1");
        if (client == null) {
            out.println("<h1> no such client! </h1>");
        } else {
            out.println("<h1> " + client.getClientId() + " </h1>");
        }
    }

}
