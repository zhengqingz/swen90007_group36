package domain;

import datasource.IdentityMap;
import datasource.OrderMapper;

public class NormalOrder extends Order {

    private boolean isRegistered;

    public NormalOrder() {
    }

    public NormalOrder(int orderId) {
        super(orderId);
    }

    public NormalOrder(int orderId, boolean isExpress, Client client,
            OrderDetail orderDetail, Destination destination,
            boolean isRegistered) {
        super(orderId, isExpress, client, orderDetail, destination);
        this.isRegistered = isRegistered;
    }

    public boolean isRegistered(boolean isInserting) {
        if(!isInserting) {
            checkAndLoad();
        }
        return isRegistered;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }
    
    @Override
    protected void checkAndLoad() {
        if (client == null || orderDetail == null || destination == null) {
            OrderMapper orderMapper = new OrderMapper();
            Order temp = orderMapper.findWithId(orderId, false);
            NormalOrder tempNormal = (NormalOrder) temp;
            isExpress = temp.isExpress(false);
            client = temp.getClient(false);
            destination = temp.getDestination(false);
            isRegistered = tempNormal.isRegistered(false);
            orderDetail = temp.getOrderDetail(false);
            IdentityMap.getInstance(this).put(orderId, this);
        }
    }

}
