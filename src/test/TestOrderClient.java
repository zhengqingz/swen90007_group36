package test;

import java.io.IOException;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Order;
import datasource.*;

@WebServlet("/testorderclient")
public class TestOrderClient extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        OrderMapper om = new OrderMapper();
        List<Order> orders = new ArrayList<>();
        orders = om.findForClient(1);
        if (orders.size() == 0) {
            out.println("<h1> this client has not orders! </h1>");
        } else {
            for (Order o : orders) {
                out.println("<h1> " + o.getOrderId() + " </h1>");
            }
        }
    }
}
