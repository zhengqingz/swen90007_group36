package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Order;
import service.EditOrderService;

import datasource.*;

@WebServlet("/testorderedit")
public class TestOrderEdit extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        EditOrderService eos = new EditOrderService();
        OrderMapper om = new OrderMapper();
        Order order = om.findWithId(2, true);
        eos.updateWeight(order, 1123123);
    }
}
