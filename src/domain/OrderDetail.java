package domain;

import datasource.IdentityMap;
import datasource.OrderMapper;

public class OrderDetail {

    private int orderDetailId;
    private int weight;
    private boolean isFragile;

    public OrderDetail(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public OrderDetail(int orderDetailId, int weight, boolean isFragile) {
        this.orderDetailId = orderDetailId;
        this.weight = weight;
        this.isFragile = isFragile;
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public int getWeight(boolean isInserting) {
        if(!isInserting) {
            checkAndLoad();
        }
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isFragile(boolean isInserting) {
        if(!isInserting) {
            checkAndLoad();
        }
        return isFragile;
    }

    public void setFragile(boolean isFragile) {
        this.isFragile = isFragile;
    }

    private void checkAndLoad() {
        if (weight == 0) {
            OrderMapper orderMapper = new OrderMapper();
            Order temp = orderMapper.findWithId(orderDetailId, false);
            OrderDetail orderdetail = temp.getOrderDetail(false);
            weight = orderdetail.getWeight(false);
            isFragile = orderdetail.isFragile(false);
            IdentityMap.getInstance(this).put(orderDetailId, this);
        }
    }

}
