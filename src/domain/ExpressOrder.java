package domain;

import datasource.IdentityMap;
import datasource.OrderMapper;

public class ExpressOrder extends Order {

    private int days;

    public ExpressOrder() {

    }

    public ExpressOrder(int orderId) {
        super(orderId);
    }

    public ExpressOrder(int orderId, boolean isExpress, Client client,
            OrderDetail orderDetail, Destination destination, int days) {
        super(orderId, isExpress, client, orderDetail, destination);
        this.days = days;
    }

    public int getDays(boolean isInserting) {
        if(!isInserting) {
            checkAndLoad();
        }
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    @Override
    protected void checkAndLoad() {
        if (client == null || orderDetail == null || destination == null) {
            OrderMapper orderMapper = new OrderMapper();
            Order temp = orderMapper.findWithId(orderId, false);
            ExpressOrder tempNormal = (ExpressOrder) temp;
            isExpress = temp.isExpress(false);
            client = temp.getClient(false);
            destination = temp.getDestination(false);
            days = tempNormal.getDays(false);
            orderDetail = temp.getOrderDetail(false);
            IdentityMap.getInstance(this).put(orderId, this);
        }
    }

}
