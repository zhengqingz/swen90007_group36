package presentation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datasource.OrderMapper;
import domain.Order;
import service.DeleteOrderService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Servlet implementation class ViewBooks
 */
@WebServlet("/delete")
public class delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int orderid = Integer.parseInt(request.getParameter("orderid"));
		System.out.println(orderid);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		OrderMapper om = new OrderMapper();
		Order order = om.findWithId(orderid, true);
		DeleteOrderService dos = new DeleteOrderService();
		dos.deleteOrder(order);
		out.println("<h1>Deleted! </h1>");
		
	}
}