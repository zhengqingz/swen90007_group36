package service;

import datasource.IdentityMap;
import datasource.UnitOfWork;
import domain.Order;
import domain.OrderDetail;

public class EditOrderService {

    public void updateWeight(Order order, int weight) {
        OrderDetail orderDetail = order.getOrderDetail(false);
        orderDetail.setWeight(weight);
        order.setOrderDetail(orderDetail);
        IdentityMap.getInstance(order).put(order.getOrderId(), order);
        IdentityMap.getInstance(orderDetail).put(order.getOrderId(),
                orderDetail);
        UnitOfWork.getInstance().commit();
    }

}
