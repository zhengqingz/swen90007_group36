package presentation;

import datasource.ClientMapper;
import datasource.OrderMapper;
import domain.Client;
import domain.Order;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/order")
public class order extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		String n = request.getParameter("username");

		ClientMapper cm = new ClientMapper();
		Client client = cm.findWithUsername(n);
		
		OrderMapper om = new OrderMapper();
		List<Order> orders = om.findForClient(client.getClientId());

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\">");
		out.println("<title></title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h2>Here are your orders.</h2>");
		out.println("");
		out.println("<table style=\"width:100%\">");
		out.println("<tr>");
		out.println("<th>Tracking No.</th>");
		out.println("<th>Destination</th> ");
		out.println("<th>Weight(kg)</th>");
		out.println("<th>Action</th>");
		out.println("</tr>");

		for (Order order : orders) {
			out.println("<tr>");
			out.println("<th>" + order.getOrderId() + "</th>");
			out.println("<th>" + order.getDestination(false).getCity() + "</th>");
			out.println("<th>" + order.getOrderDetail(false).getWeight(false) + "</th>");
			out.println("<th>");

			out.print("<form action=\"view\" method=\"post\">");
			out.print("<button type=\"submit\" name=\"orderid\" value=\"" + order.getOrderId()
					+ "\" class=\"button button2\">View</button>");
			out.print("</form>");
//View

			out.print("<form action=\"delete\" method=\"post\">");
			out.print("<button type=\"submit\" name=\"orderid\" value=\"" + order.getOrderId()
					+ "\" class=\"button button2\">Delete</button>");
			out.print("</form>");
//Delete

			out.println("<button type=\"Edit\" onclick=\"alert('Edit')\" class=\"button button2\">Edit</button>");

			out.println("</th>");
			out.println("</tr>");
		}

		out.println("<link rel='stylesheet' href='style.css'/>");
		out.println("</body>");
		out.println("</html>");

		out.close();
	}
}
