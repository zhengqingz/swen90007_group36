package datasource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Order;

public class OrderMapper {

    private NormalOrderMapper normalOrderMapper;
    private ExpressOrderMapper expressOrderMapper;

    public OrderMapper() {
        normalOrderMapper = new NormalOrderMapper();
        expressOrderMapper = new ExpressOrderMapper();
    }

    protected static final String findWithId = "SELECT * "
            + "  from APP.orders " + "  WHERE idOrder = ?";

    private static final String findOrdersForClient = "SELECT * "
            + "  from APP.orders " + "  WHERE client_id = ?";

    private static final String updateWeight = "UPDATE APP.orders "
            + "  set weight = ?" + "  where idOrder = ?";

    private static final String deleteOrder = "DELETE FROM APP.orders "
            + "  where idOrder = ?";

    public Order findWithId(int orderId, boolean lazy) {
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        Order result = null;
        try {
            findStatement = DBConnection.prepare(findWithId);
            findStatement.setInt(1, orderId);
            rs = findStatement.executeQuery();
            if (rs.next()) {
                String typecode = rs.getString(5);
                switch (typecode) {
                case NormalOrderMapper.TYPE_CODE:
                    if (lazy) {
                        result = normalOrderMapper.findInMap(orderId);
                    } else {
                        result = normalOrderMapper.findWithId(orderId, lazy);
                    }
                    break;
                case ExpressOrderMapper.TYPE_CODE:
                    if (lazy) {
                        result = expressOrderMapper.findInMap(orderId);
                    } else {
                        result = expressOrderMapper.findWithId(orderId, lazy);
                    }
                    break;
                default:
                    throw new Exception("unknow type");
                }
            }
        } catch (SQLException e) {

        } catch (Exception e) {

        }
        // IdentityMap.getInstance(result).put(clientId, result);
        return result;
    }

    public List<Order> findForClient(int clientId) {
        List<Order> result = new ArrayList<>();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = DBConnection.prepare(findOrdersForClient);
            findStatement.setInt(1, clientId);
            rs = findStatement.executeQuery();
            while (rs.next()) {
                Order temp = null;
                String typecode = rs.getString(5);
                int orderId = rs.getInt(1);
                switch (typecode) {
                case NormalOrderMapper.TYPE_CODE:
                    temp = normalOrderMapper.findInMap(orderId);
                    result.add(temp);
                    break;
                case ExpressOrderMapper.TYPE_CODE:
                    temp = expressOrderMapper.findInMap(orderId);
                    result.add(temp);
                    break;
                default:
                    throw new Exception("unknow type");
                }
            }
        } catch (SQLException e) {

        } catch (Exception e) {

        }
        return result;
    }

    public void update(Order order) {
        PreparedStatement updateStatement = null;
        try {
            updateStatement = DBConnection.prepare(updateWeight);
            updateStatement.setInt(1,
                    order.getOrderDetail(false).getWeight(false));
            updateStatement.setInt(2, order.getOrderId());

            updateStatement.execute();

        } catch (SQLException e) {

        }
    }

    public void delete(Order order) {
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = DBConnection.prepare(deleteOrder);
            deleteStatement.setInt(1, order.getOrderId());

            deleteStatement.execute();
        } catch (SQLException e) {

        }
    }

}
