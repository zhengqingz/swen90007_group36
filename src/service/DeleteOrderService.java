package service;

import datasource.IdentityMap;
import datasource.UnitOfWork;
import domain.Order;
import domain.OrderDetail;

public class DeleteOrderService {

    public void deleteOrder(Order order) {
        OrderDetail orderDetail = order.getOrderDetail(false);
        IdentityMap.getInstance(orderDetail)
                .remove(orderDetail.getOrderDetailId());
        IdentityMap.getInstance(order).remove(order.getOrderId());
        UnitOfWork.getInstance().registerDeleted(order);
        UnitOfWork.getInstance().commit();
    }

}
