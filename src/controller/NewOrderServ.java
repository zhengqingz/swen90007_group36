package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/neworderform")
public class NewOrderServ extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// read form fields
		String destination = request.getParameter("destination");
		String weight = request.getParameter("weight");
		String fragile = request.getParameter("fragile");
		String delivery = request.getParameter("delivery");
		String days = request.getParameter("days");
		String register = request.getParameter("register");
		if (register == null) {
			register = "False";
		}
		// String register = request.getParameter("register");

		System.out.println(destination + "," + weight + "," + fragile + "," + delivery + "," + days);

		// do some processing here...

		// get response writer
		PrintWriter writer = response.getWriter();

		// build HTML code
		String htmlRespone = "<html>";
		htmlRespone += "<h2>Destination:" + destination + "<br/>";
		htmlRespone += "<h2>Weight: " + weight + "<br/>";
		htmlRespone += "<h2>Fragile: " + fragile + "<br/>";
		htmlRespone += "<h2>Delivery: " + delivery + "<br/>";
		htmlRespone += "<h2>Days: " + days + "<br/>";
		htmlRespone += "<h2>Registered: " + register + "<br/>";
		// return response
		writer.println(htmlRespone);

	}

}