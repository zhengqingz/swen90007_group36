package service;

import datasource.ClientMapper;
import domain.Client;

public class RegisterAndLoginService {

    public boolean login(String username, String password) {
        ClientMapper clientMapper = new ClientMapper();
        Client temp = clientMapper.findWithUsername(username);
        if (temp != null && temp.getPassword().equals(password)) {
            return true;
        }
        return false;
    }

    public boolean register(String username, String password) {
        ClientMapper clientMapper = new ClientMapper();
        Client temp = clientMapper.findWithUsername(username);
        if (temp == null) {
            Client client = new Client(0, username, password);
            client.setClientId(clientMapper.insert(client));
            // IdentityMap.getInstance(client).put(client.getClientId(), client);
            return true;
        }
        return false;
    }
}
