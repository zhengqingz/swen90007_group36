package test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Client;
import domain.Destination;
import service.RegisterAndLoginService;

@WebServlet("/testregister")
public class TestRegister extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String username = "client5";
        String password = "client5";
        RegisterAndLoginService service = new RegisterAndLoginService();
        boolean registersuccess = service.register(username, password);
        if (registersuccess) {
            if (service.login(username, password)) {
                out.println("<h1> login success! </h1>");
            }
        } else {
            out.println("<h1> register fail! </h1>");
        }
    }

}
